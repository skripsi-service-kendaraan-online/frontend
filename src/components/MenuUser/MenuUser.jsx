import classNames from "classnames";
import style from "./MenuUser.module.css";
import {Row, Col, Card} from "react-bootstrap";
import iconCarService from "../../assets/images/iconCarService.png";

function MenuUser(){
    return(
        <>
           <div className="container my-3 d-flex">
            <Row className="w-100 d-flex justify-content-around m-auto">
                <Col xs={12} className="d-flex">
                     <h1 className={classNames(style.textHeader)}>Select Service</h1>
                </Col>
                  <Col xs={3} className={classNames(style.card,"card d-flex justify-content-center my-2")}>
                     <div className={classNames("d-flex flex-column align-items-center")}>
                        <img src={iconCarService} className={classNames(style.imgMenu)}/>
                        <div className={classNames(style.textCard)}>Panggil Mekanik</div>
                     </div>
                  </Col>
                  <Col xs={3} className={classNames(style.card,"card d-flex justify-content-center my-2")}>
                     <div className={classNames("d-flex flex-column justify-content-center align-items-center" )}>
                        <img src={iconCarService} className={classNames(style.imgMenu)}/>
                        <div className={classNames(style.textCard)}>Jadwalkan Service</div>
                     </div>
                  </Col>
                  <Col xs={3} className={classNames(style.card,"card d-flex justify-content-center my-2")}>
                     <div className={classNames("d-flex flex-column align-items-center")}>
                        <img src={iconCarService} className={classNames(style.imgMenu)}/>
                        <div className={classNames(style.textCard)}>Jadwalkan Service</div>
                     </div>
                  </Col>
                  <Col xs={3} className={classNames(style.card,"card d-flex justify-content-center my-2")}>
                     <div className={classNames("d-flex flex-column align-items-center")}>
                        <img src={iconCarService} className={classNames(style.imgMenu)}/>
                        <div className={classNames(style.textCard)}>Jadwalkan Service</div>
                     </div>
                  </Col>
            </Row>
           </div>
        </>
    )
}

export default MenuUser;