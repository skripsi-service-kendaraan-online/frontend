import {BiHome} from "react-icons/bi";
import {GiSteeringWheel} from "react-icons/gi";
import {AiOutlineHistory} from "react-icons/ai";
export let UserMenuItems = [
    {
        title: "Home",
        icon: <BiHome />,
        activeIcon: <BiHome />
    },
    {
        title: "Vehicle",
        icon: <GiSteeringWheel/>,
        activeIcon: <GiSteeringWheel/>
    },
    {
        title: "History",
        icon: <AiOutlineHistory/>,
        activeIcon: <AiOutlineHistory/>
        
    }
]