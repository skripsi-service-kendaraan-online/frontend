export let EmployeeMenuItems = [
    {
        title: "DASHBOARD",
        url: "/dashboard",
        cName: "nav_links",
    },
    {
        title: "PROFIL SAYA",
        url: "/karyawan/profile",
        cName: "nav_links",
    },
    {
        title: "DAFTAR SERVIS",
        url: "/karyawan/servis/pending",
        cName: "nav_links",
    },
];
