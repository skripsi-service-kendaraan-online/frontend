export let MitraMenuItems = [
    {
        title: "DASHBOARD",
        url: "/dashboard",
        cName: "nav_links",
    },
    {
        title: "PROFIL SAYA",
        url: "/mitra/profile",
        cName: "nav_links",
    },
    {
        title: "PACKAGES",
        url: "/mitra/packages",
        cName: "nav_links",
    },
];


