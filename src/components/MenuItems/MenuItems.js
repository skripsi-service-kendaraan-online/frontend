export let MenuItems = [
    {
        title: "Dashboard",
        url: "/dashboard",
        cName: "navLinks",
    },
    {
        title: "Profile Saya",
        url: "/user",
        cName: "navLinks",
    },
    {
        title: "Riwayat Service",
        url: "/user/order/riwayat-servis",
        cName: "navLinks",
    }
]