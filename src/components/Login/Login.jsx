import { Card, Button, Form, Alert, FormGroup, FormLabel, FormControl } from "react-bootstrap";
import classNames from "classnames";
import style from "./Login.module.css";
import { Link } from "react-router-dom";
import { BsGoogle } from "react-icons/bs";


export default function Login({ handleLoginSubmitted, error, email, password, setEmail, setPassword, loading, handleSignInWithGoogleAccount }){
    return(
        <>
        <div className={classNames("w-100 vh-100 d-flex justify-content-center align-items-center", style.loginPage)}>
            <Card className={classNames(style.loginCard, "mx-2")}>
                {error & <Alert variant="danger">{error}</Alert>}
                <Card.Body>
                    <Button className={classNames("w-100 mb-2 fw-bold text-uppercase")} variant="info" onClick={handleSignInWithGoogleAccount}> 
                        <BsGoogle/>
                    </Button>
                    <Form onSubmit={handleLoginSubmitted}>
                        <div className="py-2">
                            <Form.Label>Email</Form.Label>
                            <FormControl
                                id="email"
                                name="email"
                                type="email"
                                placeholder="Your Email"
                                value={email}
                                onChange={(e)=>{
                                    setEmail(e.target.value)
                                }}
                            />
                        </div>
                        <div className="py-2">
                            <Form.Label>Password</Form.Label>
                            <FormControl
                                id="password"
                                name="password"
                                type="password"
                                placeholder="Your Password"
                                value={password}
                                onChange={(e)=>{
                                    setPassword(e.target.value)
                                }}
                            />
                        </div>
                        <Button className="w-100 mt-2" type="submit" variant="warning">
                            {loading ? <div className="spinner-border spinner-border-sm" role="status"></div> : "Login"}
                        </Button>
                    </Form>
                    <div className="w-100 text-center mt-3">
                        Belum Punya Akun ? {""}
                        <Link to="/register" className="text-primary">
                            Register
                        </Link>
                    </div>
                    <div className="w-100 text-center mt-1">
                        <Link to="/forgot-password" className="text-danger">
                            Lupa kata sandi?
                        </Link>
                    </div>
                </Card.Body>
            </Card>
        </div>
        </>
    )
}
