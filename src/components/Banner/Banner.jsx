import Carousel from 'react-bootstrap/Carousel';
import BannerImg from "../../assets/images/Banner.png";
import { Row, Col} from "react-bootstrap";

function Banner(){
    return(
        <div className="container w-100 m-auto">
        <Carousel className="row m-auto">
            <Carousel.Item className="col">
               <img
                className="d-block w-100"
                src={BannerImg}
                alt="First slide"
               />
            </Carousel.Item>
        </Carousel>
        </div>
    )
}

export default Banner