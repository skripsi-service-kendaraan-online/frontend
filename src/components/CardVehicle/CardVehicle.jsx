import { Row, Col, Card } from "react-bootstrap";
import style from "./CardVehicle.module.css";
import Car from "../../assets/images/car.png";
import classNames from "classnames";
import { BsDot, BsThreeDotsVertical, BsTrash } from "react-icons/bs";

function CardVehicle({vehicle}){
    return(
        <div className="container d-flex justify-content-center">
        <Row className={classNames(style.card,"card container d-flex")}>
            <Col xs={12} className={classNames("d-flex align-items-center")}>
                <div className="d-flex flex-column w-100">
                    <h5 className={classNames(style.textCard)}>{vehicle.brand.brandName}</h5>
                    <h5 className={classNames(style.textBody)}>{vehicle.modelName}</h5>
                    <h5 className={classNames(style.textBody)}>{vehicle.modelYear}</h5>
                </div>
                <div className="d-flex flex-column">   
                    <img src={Car} alt="" className={classNames(style.imgCard)}/>
                </div>
            </Col>
            <Col xs={12} className={classNames(style.btn,"d-flex flex-column")}>
                <div className={classNames(style.cardButton,"d-flex justify-content-between align-items-center")}>
                    <input type="submit" className={classNames(style.btnAdd,"border-0 btn text-uppercase" )} value="Order Service"/>
                    <input type="submit" className={classNames(style.btnDelete,"border-0 btn text-uppercase" )} value="delete"/>
                </div>
            </Col>
        </Row>
        </div>
    )
}

export default CardVehicle;