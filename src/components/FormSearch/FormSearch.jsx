import {Row, Col} from "react-bootstrap";

function FormSearch(){
    return(
        <div className="container">
        <Row className="w-100 d-flex my-3 m-auto">
        <Col className="w-100 m-auto">
            <form action="m-auto">
               <input 
                type="text"
                className="form-control"
                placeholder="Search for a vehicle service"
               />
            </form>
        </Col>
      </Row>
      </div>
    )
}

export default FormSearch;