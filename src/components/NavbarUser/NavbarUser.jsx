import classNames from "classnames";
import style from "./NavbarUser.module.css";
import { GiHamburgerMenu } from "react-icons/gi";
import { ImCross } from "react-icons/im";
import { IoMdLogOut } from "react-icons/io";
import { useState } from "react";
import {Button} from "react-bootstrap";
import {useAuth} from "../../context/AuthContext";
import {MenuItems} from "../MenuItems/MenuItems";
import {MitraMenuItems} from "../MenuItems/MitraMenuItems";
import {EmployeeMenuItems} from "../MenuItems/EmployeeMenuItems";
import {Link, useNavigate} from "react-router-dom";
import swal from "sweetalert";

function NavbarUser(props) {
    const role = localStorage.getItem("ROLE");
    const [clicked, setClicked] = useState(false);
    const {currentUser, logout } = useAuth();
    const navigate = useNavigate();

    const handleClick = () =>{
        setClicked(!clicked);
    }

    const handleLogoutButton = async(e) => {
      e.preventDefault();
      try {
        swal({
          title: "Konfirmasi Logout?",
          icon: "warning",
          buttons: true,
          dangerMode: true
        }).then((willDelete) => {
          if(willDelete){
            logout().then(() => {
              swal("Logout success",{
                icon: "success"
              }).then(() => {
                navigate("/login");
              })
            })
          }
        })
      } catch (error) {
        console.log(error.message);
      }
    }

    let filteredMenuItems = !currentUser
      ? MenuItems.filter((obj) => {
        return obj.condition === "logout"
    })
    :MenuItems;

    return(
      <nav className={classNames(style.NavbarItems)}>
          <Link to="/">
            <h1 className={classNames(style.navbarLogo)}>
                Bengkel Online
            </h1>
          </Link>
        <div className={classNames(style.menuIconWrapper)} onClick={handleClick}>
            {clicked === true ? <ImCross className={classNames(style.menuIcon)}/> : <GiHamburgerMenu className={classNames(style.menuIcon)}/>}
        </div>
        <ul className={clicked? classNames(style.navMenu, style.active) : classNames(style.navMenu)} >
          {currentUser ? (
            <></>
          ):(
            <>
            <li>
              <Button className={classNames(style.btn, style.btnLogin)}>LOGIN</Button>
            </li>
            <li>
              <Button className={classNames(style.btn, style.Register)}>REGISTER</Button>
            </li>
          </>
          )}
          {role === "Member" && (
            <>
              {filteredMenuItems.map((item, index) => {
                return(
                  <li className={classNames(style.navLinks, "text-center")} key={index}>
                    <Link to={item.url}>
                      <p>{item.title}</p>
                    </Link>
                  </li>
                )
              })}
            </>
          )}
           {role === "Mitra" && (
            <>
              {MitraMenuItems.map((item, index) => {
                return(
                  <li className={classNames(style.navLinks, "text-center")} key={index}>
                    <Link to={item.url}>
                      <p>{item.title}</p>
                    </Link>
                  </li>
                )
              })}
            </>
          )}

           {role === "Mitra Employee" && (
            <>
              {EmployeeMenuItems.map((item, index) => {
                return(
                  <li className={classNames(style.navLinks, "text-center")} key={index}>
                    <Link to={item.url}>
                      <p>{item.title}</p>
                    </Link>
                  </li>
                )
              })}
            </>
          )}

          {currentUser ? (
            <>
              <li>
                <div className={classNames(style.logout, "text-uppercase")} onClick={handleLogoutButton}>
                  {clicked && <IoMdLogOut/>}
                </div>
              </li>
            </>
          ):(
            <></>
          )}
        </ul>
      </nav>
    )
}

export default NavbarUser;