import { useEffect, useState, Fragment } from "react";
import { useAuth } from "../../context/AuthContext";
import { Row, Col, Card} from "react-bootstrap";
import classNames from "classnames";
import style from "./Header.module.css";
import Ellipse from "../../assets/images/Ellipse.png";
import { Link } from "react-router-dom";
import axios from "axios";

function Header(){
  const API_URL = process.env.REACT_APP_API_URI;
  const ACCESS_TOKEN = localStorage.getItem("ACCESS_TOKEN");
  const {currentUser} = useAuth();
  const userId = currentUser?.uid;

  const [dataUser, setDataUser] = useState({});
  const [checkUserBiodata, setCheckUserBiodata] = useState(false);

  const checkRole = localStorage.getItem("ROLE");

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
      if(!!userId){
          fetchUserBiodata();
      }
  },[userId]);

  const fetchUserBiodata = () => {
      try {
          axios
              .get(`${API_URL}/api/v1/userbiodatas/`,{headers: {Authorization: ACCESS_TOKEN}})
              .then((res) => {
                  setCheckUserBiodata(true);
                  setIsLoading(true);
                  setDataUser(res.data);
              })
              .catch((err) => {
                  if(err.response.status === 401){
                      setIsLoading(true);
                      console.log("userbio not found");
                      return;
                  }
                  if(err.response.status === 500){
                      console.log("internal server error");
                      return;
                  }
              })
      } catch (error) {
          console.log(error);
      }
    }
    return(
        <>
        <div className="w-100 container my-3">
            <Row className="w-100 d-flex m-auto justify-content-between">
                <Col xs={12} className="d-flex justify-content-between align-items-center">
                  <div className="d-flex flex-column">
                  <h1 className={classNames(style.cardTitle,"")}>Welcome, {dataUser.userBiodata?.fullName}</h1>
                  {checkUserBiodata === false && (
                    <>
                       <span className="text-danger text-center">Anda belum melengkapi biodata!</span>
                       <Link to="/user/tambah-biodata">
                            <div className="text-center text-primary text-decoration-underline">Lengkapi!</div>
                       </Link>
                    </>
                  )}
                  <h1 className={classNames(style.cardText,"img my-2")}>{dataUser.userAddress?.city.cityName}</h1>
                  </div>
                  <div className="d-flex">
                      <img src={Ellipse} alt="" className={classNames(style.imgLogo)}/>
                  </div>
                </Col>
                <Col xs={4} className="d-flex">
                </Col>
            </Row>
        </div>
        </>
    )
}
export default Header;