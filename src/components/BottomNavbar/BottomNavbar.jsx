import BottomNavigation from 'reactjs-bottom-navigation';
import { UserMenuItems } from "../MenuItems/UserMenuItems";

function BottomNavbar(){
    
    return(
        <div>
        <BottomNavigation 
            items={UserMenuItems}
            defaultSelected={0}
            onItemClick={(item) => console.log(item)}
        />
        </div>
    )
}

export default BottomNavbar;