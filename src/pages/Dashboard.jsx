import { useEffect, useState, Fragment } from "react";
import Header from "../components/Header/Header";
import Banner from "../components/Banner/Banner";
import MenuUser from "../components/MenuUser/MenuUser";
import FormSearch from "../components/FormSearch/FormSearch";
import { useAuth } from "../context/AuthContext";
import axios from "axios";
import BottomNavbar from "../components/BottomNavbar/BottomNavbar";

function Dashboard(){
    return(
        <>
        <Fragment>
            <BottomNavbar/>
            <Header/>
            <FormSearch/>
            <Banner/>
            <MenuUser/>
        </Fragment>       
        </>
    )
}

export default Dashboard;