import { useEffect, useState } from "react";
import Header from "../components/Header/Header";
import CardVehicle from "../components/CardVehicle/CardVehicle";
import axios from "axios";
import BottomNavbar from "../components/BottomNavbar/BottomNavbar";
import swal from "sweetalert";

function Garasi(){
    const ACCESS_TOKEN = localStorage.getItem("ACCESS_TOKEN");
    const API_URL = process.env.REACT_APP_API_URI;

    const [stateStatus, setStateStatus] = useState(false);

    const [vehicles, setVehicles] = useState([]);

    useEffect(() =>{
        fetchAllVehicle();
    }, [stateStatus]);
    
    const fetchAllVehicle = () =>{
        try {
            axios
                .get(`${API_URL}/api/v1/vehicles/`,{headers:{Authorization: ACCESS_TOKEN}})
                .then((res) => {
                    return setVehicles(res.data.data.vehicles);
                    console.log(res);
                    console.log(vehicles)
                })
                .catch((err) =>{
                    if(err.response.status === 401){
                        console.log("Kendaraan tidak ditemukan");
                        return;
                    }
                    console.log(err);
                    if(err.response.status === 500){
                        console.log(err);
                        swal("Kendaraan tidak dapat dihapus, karena sedang dalam order perbaikan!", {
                            icon: "warning",
                        });
                    }
                })
        } catch (error) {
            console.log(error.message);
        }
    }
    return(
        <div>
            <BottomNavbar/>
            <Header/>
            {vehicles.map((vehicle, index) => {
                return(
                <div key={index}>
                    <CardVehicle vehicle={vehicle}/>
                </div>
                )
            })}
        </div>
    )
}

export default Garasi;
