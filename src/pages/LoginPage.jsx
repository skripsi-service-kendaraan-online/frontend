import { useState } from "react";

import Login from "../components/Login/Login";
//import Navbar from "../components/Navbar/Navbar";
import NavbarUser from "../components/NavbarUser/NavbarUser";
import { useAuth } from "../context/AuthContext";

import { useNavigate } from "react-router-dom";
import axios from "axios";
import { Navbar } from "react-bootstrap";

export default function LoginPage(){
    const API_URL = process.env.REACT_APP_API_URI;

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
    const {login, signInWithGoogle} = useAuth();
    const navigate = useNavigate();

    const handleLoginSubmitted = async (e) => {
        e.preventDefault();

        try{
            setError("");
            setLoading(true);
            setTimeout(() => {
                setLoading(false);
                login(email,password)
                    .then((response) =>{
                        setTimeout(() => {
                            axios
                                .get(`${API_URL}/api/v1/user/role`, {headers: {Authorization: `Bearer ${response.user.accessToken}`}})
                                .then((response) => {
                                    localStorage.setItem("ROLE", `${response.data.Role}`);
                                    localStorage.setItem("isAuthenticated", "true");
                                })
                                .then(() => {
                                    navigate("/dashboard");
                                })
                                .catch((err) => {
                                    console.log(err);
                                });
                        }, 1000)

                    })
                    .catch((err) => {
                        console.log(err.message);
                        setError("Email Tidak Terdaftar");
                    });
            }, 1000);
        } catch(err) {
            console.log(err.message);
            return setError(err.message);
        }
    }

    const handleSignInWithGoogleAccount = async (e) => {
        e.preventDefault();
        try {
            signInWithGoogle()
                .then((response) => {
                    console.log(response);
                    navigate("/");
                })
                .catch((err) => {
                    console.log(err.message);
                    setError(err.message);
                })
        } catch (error) {
            return setError("Failed To Sign Up");
        }
    };

    return(
        <>
            <NavbarUser/>
            <Login
                handleLoginSubmitted={handleLoginSubmitted}
                error={error}
                email={email}
                setEmail={setEmail}
                password={password}
                setPassword={setPassword}
                loading={loading}
                handleSignInWithGoogleAccount={handleSignInWithGoogleAccount}
            />
        </>
    )
}

